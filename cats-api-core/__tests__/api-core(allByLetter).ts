import Client from '../../dev/http-client';
import type { CatMinInfo, Cat, CatsList } from '../../dev/types';

const HttpClient = Client.getInstance();

describe('API котиков: allByLetter', () => {
    const cats: CatMinInfo[] = [
        { name: 'Арнольд', description: 'Кот', gender: 'male' },
        { name: 'Барсик', description: 'Кот', gender: 'male' },
    ];

    let addedCatIds: number[] = [];

    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });

            addedCatIds = (add_cat_response.body as CatsList).cats.map((cat: Cat) => cat.id);
        } catch (error) {
            throw new Error('Не удалось создать котиков для автотестов!');
        }
    });

    afterAll(async () => {
        await Promise.all(
            addedCatIds.map(id =>
                HttpClient.delete(`core/cats/${id}/remove`, {
                    responseType: 'json',
                })
            )
        );
    });

    it('Получение списка котов сгруппированных по группам с лимитом', async () => {
        const limit = 5;
        const order = 'asc';
        const gender = 'male';
        const response = await HttpClient.get(`core/cats/allByLetter?limit=${limit}&order=${order}&gender=${gender}`, {
            responseType: 'json',
        });

        expect(response.statusCode).toEqual(200);

        const responseBody = response.body as {
            groups: { title: string, count_in_group: number, cats: Cat[] }[];
        };

        responseBody.groups.forEach(group => {
            expect(group.cats.length).toBeLessThanOrEqual(limit);

            expect(group).toMatchObject({
                title: expect.any(String),
                count_in_group: expect.any(Number),
                cats: expect.any(Array),
            });

            group.cats.forEach(cat => {
                expect(cat).toMatchObject({
                    id: expect.any(Number),
                    name: expect.any(String),
                    gender: expect.any(String),
                    likes: expect.any(Number),
                    dislikes: expect.any(Number),
                });

                if (cat.message !== undefined) {
                    expect(cat.message).toBeDefined();
                }
            });
        });
    });
});
