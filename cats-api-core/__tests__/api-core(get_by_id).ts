import Client from '../../dev/http-client';
import { CatMinInfo, CatsList, Cat } from '../../dev/types';

const HttpClient = Client.getInstance();
let catId: number;
const fakeId = 'fakeId';
const testCat: CatMinInfo = { name: 'Тесткотик', description: '', gender: 'male' }; // Имя с маленькой буквы

describe('API котиков: get_by_id', () => {
    beforeAll(async () => {
        const cats: CatMinInfo[] = [testCat];
        const addCatResponse = await HttpClient.post('core/cats/add', {
            responseType: 'json',
            json: { cats },
        });
        const responseBody = addCatResponse.body as CatsList;
        if (responseBody.cats && responseBody.cats.length > 0) {
            catId = responseBody.cats[0].id;
        } else {
            throw new Error('Не получилось получить id тестового котика!');
        }
    });

    afterAll(async () => {
        if (catId) {
            try {
                await HttpClient.delete(`core/cats/${catId}/remove`, {
                    responseType: 'json',
                });
            } catch (error) {
                console.error(`Ошибка при удалении кота с id ${catId}:`, error);
            }
        }
    });

    it('Поиск существующего котика по id', async () => {
        const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
            responseType: 'json',
        });
        const responseBody = response.body as { cat: Cat };
        expect(response.statusCode).toEqual(200);
        expect(responseBody.cat.id).toEqual(catId);
        expect(responseBody.cat.name).toEqual(testCat.name);
        expect(responseBody.cat.description).toEqual(testCat.description);
        expect(responseBody.cat.gender).toEqual(testCat.gender);
    });

    it('Поиск котика с некорректным id', async () => {
        await expect(
            HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
                responseType: 'json',
            })
        ).rejects.toThrowError('Response code 400 (Bad Request)');
    });
});
