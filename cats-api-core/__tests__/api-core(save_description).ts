import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList, Cat } from '../../dev/types';

const HttpClient = Client.getInstance();

describe('API котиков: save_description', () => {
    let catId: number;

    const cats: CatMinInfo[] = [
        { name: 'Тестовый Кот', description: '', gender: 'male' },
    ];

    beforeAll(async () => {
        try {
            const addCatResponse = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });

            const responseBody = addCatResponse.body as CatsList;

            if (responseBody.cats && responseBody.cats.length > 0) {
                catId = responseBody.cats[0].id;
            } else {
                throw new Error('Не получилось получить id тестового котика!');
            }
        } catch (error) {
            console.error('Ошибка при добавлении кота:', error);
            throw new Error('Не удалось создать котика для автотестов!');
        }
    });

    afterAll(async () => {
        try {
            await HttpClient.delete(`core/cats/${catId}/remove`, {
                responseType: 'json',
            });
        } catch (error) {
            console.error('Ошибка при удалении кота:', error);
        }
    });

    it('Добавление описания котику', async () => {
        const description = 'Очень хороший котик';
        try {
            const response = await HttpClient.post('core/cats/save-description', {
                responseType: 'json',
                json: { catId, catDescription: description },
            });

            expect(response.statusCode).toEqual(200);
            const responseBody = response.body as Cat;
            expect(responseBody.description).toEqual(description);
        } catch (error) {
            throw new Error(`Ошибка при добавлении описания котику: ${error.message}`);
        }
    });
});




